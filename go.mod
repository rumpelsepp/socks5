module codeberg.org/rumpelsepp/socks5

go 1.17

require (
	codeberg.org/rumpelsepp/helpers v0.0.0-20211020091314-b9b064cf8c8a
	github.com/Fraunhofer-AISEC/penlogger v0.0.0-20210914113712-8a2b1758b080
)

require (
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf // indirect
	github.com/google/uuid v1.3.0 // indirect
	golang.org/x/crypto v0.0.0-20211117183948-ae814b36b871 // indirect
	golang.org/x/sys v0.0.0-20211117180635-dee7805ff2e1 // indirect
)
